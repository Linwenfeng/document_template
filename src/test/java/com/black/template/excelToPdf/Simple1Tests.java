package com.black.template.excelToPdf;

import com.itextpdf.text.DocumentException;
import org.junit.Test;
import java.io.*;
import java.util.Arrays;

public class Simple1Tests {
    @Test
    public void testCase1OfSingle() throws IOException, DocumentException {
        String fileIn = "sample1/111.xlsx";
//         fileIn = "sample1/case1.xls";
        String fileOut = "E:\\111.pdf";
        InputStream in = this.getClass().getResourceAsStream(fileIn);
        InputStream in1 = this.getClass().getResourceAsStream(fileIn);
        Excel2Pdf excel2Pdf = new Excel2Pdf(Arrays.asList(
                        new ExcelObject(in,0),new ExcelObject(in1,1)
        ), new FileOutputStream(fileOut));
        excel2Pdf.convert();
    }
    @Test
    public void test111() throws IOException, DocumentException {
        String fileIn = "sample1/111.xls";
        InputStream in = this.getClass().getResourceAsStream(fileIn);
        Excel2Pdf excel2Pdf = new Excel2Pdf(Arrays.asList(
                new ExcelObject(in,0)
        ), new FileOutputStream(fileOut(fileIn)));
        excel2Pdf.convert();
    }
    @Test
    public void testCase1() throws IOException, DocumentException {
        String fileIn = "E://template_result.xls";
        InputStream in = new FileInputStream(fileIn);
        Excel2Pdf excel2Pdf = new Excel2Pdf(Arrays.asList(
                new ExcelObject(in,0)
        ), new FileOutputStream("E://1.pdf"));
        excel2Pdf.convert();
    }
    @Test
    public void testCase5() throws IOException, DocumentException {
        String fileIn = "sample1/222.xlsx";
        InputStream in = this.getClass().getResourceAsStream(fileIn);
        Excel2Pdf excel2Pdf = new Excel2Pdf(Arrays.asList(
                new ExcelObject(in)
        ), new FileOutputStream(fileOut(fileIn)));
        excel2Pdf.convert();
    }

    private File fileOut(String fileIn) {
        String uri = this.getClass().getResource(fileIn).getPath();
        String fileOut = uri.replaceAll(".xls$|.xlsx$", ".pdf");
        return new File(fileOut.substring(1));
    }

    public static void main(String[] args) {
        System.out.println(1|2);
    }
}