package com.black.template.excelToPdf;

import com.black.template.DocumentTemplateApplication;
import com.black.template.service.handler.TemplateHandlerService;
import com.black.template.vo.DocumentTemplateVo;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.*;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = DocumentTemplateApplication.class)
public class DocumentTemplateApplicationTest {
    @Autowired
    TemplateHandlerService templateHandlerService;

    @Test
    public void generateDocumentDataTest() throws Exception {
        DocumentTemplateVo documentTemplateVo = new DocumentTemplateVo();
        Map<String,Object> fillData = new HashMap<>();
        List<Employee> employees = new ArrayList<>();
        for(int i =0;i<5;i++){
            Employee employee = new Employee();
            employee.setBirthDate(new Date());
            employee.setName("A"+i);
            employee.setBonus(1);
            employee.setPayment(1);
            employees.add(employee);
        }
        fillData.put("employees",employees);
        documentTemplateVo.setFillData(fillData);
        String documentData = templateHandlerService.generateDocumentData(documentTemplateVo);
        System.out.println("------------------>"+documentData);
    }
}
