package com.black.template.excelToPdf;

import lombok.Data;

import java.util.Date;

@Data
public class Employee {
    private String name;
    private Date birthDate;
    private int payment;
    private int bonus;
}
