package com.black.template.vo;

import com.black.template.po.Template;
import lombok.Data;

import java.io.InputStream;

@Data
public class TemplateVo {

    private Template template;

    private InputStream inputStream;
}
