package com.black.template.vo;

import lombok.Data;

@Data
public class FileVo {
    private String key;
    private String data;
}
