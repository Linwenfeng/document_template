package com.black.template.vo;

import lombok.Data;

import java.util.List;
import java.util.Map;
@Data
public class DocumentTemplateVo {

    /**模板id**/
    private String templateId;

    /**模板填充数据**/
    private Map<String,Object> fillData;

    /**文件**/
    private List<FileVo> fileData;
}
