package com.black.template.spire;

import com.spire.xls.FileFormat;
import com.spire.xls.Workbook;

import java.io.*;

public class ExcelToPdf {

    public static void convertToPdf(InputStream inputStream, OutputStream outputStream){
        Workbook wb = new Workbook();
        wb.loadFromStream(inputStream);
        wb.saveToStream(outputStream, FileFormat.PDF);
    }

}
