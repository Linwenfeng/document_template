package com.black.template.spire;

import com.spire.doc.Document;
import com.spire.doc.FileFormat;

import java.io.*;

public class WordToPdf {

    public static void convertToPdf(InputStream inputStream, OutputStream outputStream){
        Document document = new Document();
        document.loadFromStream(inputStream, FileFormat.Auto);
        document.saveToFile(outputStream,FileFormat.PDF);
    }
}
