package com.black.template.excelToPdf;

import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import java.io.IOException;
import java.io.InputStream;


public class Excel {

    protected Workbook wb;
    protected Sheet sheet;

    public Excel(InputStream is) {
        try {
            this.wb = WorkbookFactory.create(is);
            this.sheet = wb.getSheetAt(wb.getActiveSheetIndex());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public Excel(InputStream is,int sheetIndex) {
        try {
            this.wb = WorkbookFactory.create(is);
            this.sheet = wb.getSheetAt(sheetIndex);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public Sheet getSheet() {
        return sheet;
    }

    public Workbook getWorkbook(){
        return wb;
    }
}
