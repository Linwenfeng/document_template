package com.black.template.po;

import lombok.Data;

@Data
public class Template {

    /**模板Id**/
    private String templateId;

    /**模板名称**/
    private String templateName;

    /**模板类型**/
    private String templateType;

    /**转换**/
    private String toChange;

    /**路径**/
    private String path;

    /**备注**/
    private String remark;
}
