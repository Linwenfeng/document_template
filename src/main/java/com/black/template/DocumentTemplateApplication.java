package com.black.template;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DocumentTemplateApplication {

    public static void main(String[] args) {
        SpringApplication.run(DocumentTemplateApplication.class, args);
    }
}
