package com.black.template.service.handler;

import com.black.template.vo.DocumentTemplateVo;


public interface TemplateHandlerService {
    String generateDocumentData(DocumentTemplateVo documentTemplateVo) throws Exception;

    DocumentTemplateVo fileDataHandler(DocumentTemplateVo documentTemplateVo);
}
