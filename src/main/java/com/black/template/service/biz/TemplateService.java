package com.black.template.service.biz;

import com.black.template.vo.TemplateVo;

import java.io.FileNotFoundException;

public interface TemplateService {
    TemplateVo getTemplateVoById(String templateId) throws FileNotFoundException;
}
