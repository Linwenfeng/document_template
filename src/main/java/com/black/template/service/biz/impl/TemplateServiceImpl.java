package com.black.template.service.biz.impl;

import com.black.template.po.Template;
import com.black.template.service.biz.TemplateService;
import com.black.template.vo.TemplateVo;
import org.springframework.stereotype.Service;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

@Service
public class TemplateServiceImpl implements TemplateService {

    @Override
    public TemplateVo getTemplateVoById(String templateId) throws FileNotFoundException {
        TemplateVo templateVo = new TemplateVo();
        Template template = new Template();
        template.setPath("E://template.xls");
        template.setTemplateType("Excel");
        templateVo.setTemplate(template);
        templateVo.setInputStream(new FileInputStream(template.getPath()));
        return templateVo;
    }
}
