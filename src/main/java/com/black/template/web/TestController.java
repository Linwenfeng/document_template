package com.black.template.web;

import com.black.template.service.handler.TemplateHandlerService;
import com.black.template.vo.DocumentTemplateVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("test")
public class TestController {

    @Autowired
    TemplateHandlerService templateHandlerService;

    @RequestMapping("change")
    public DocumentTemplateVo test(@RequestBody DocumentTemplateVo documentTemplateVo){
        return templateHandlerService.fileDataHandler(documentTemplateVo);
    }
}
